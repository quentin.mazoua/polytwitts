'use strict';

module.exports = app => {

    var mainController = require('../controllers/main-controller.js');
    var userController = require('../controllers/user-controller.js');

    app.route('/')
        .get(mainController.indexPage);

    app.route('/search/:q')
        .get(mainController.searchTwitts);

    app.route('/users/auth')
        .post(userController.authUser);

    app.route('/users')
        .post(userController.createUser);

    app.route('/users/:user/subjects')
        .get(userController.getSavedSubjects)
        .delete(userController.deleteSavedSubject)
        .post(userController.saveSubject);

    app.route('/users/:user/tweets')
        .post(userController.saveTweet)
        .get(userController.getSavedTweets);

    app.route('/users/:user/accounts')
        .post(userController.saveAccount)
        .get(userController.getSavedAccounts);
}