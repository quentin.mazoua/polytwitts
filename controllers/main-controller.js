'use strict';

const Twitter = require('twitter-node-client').Twitter;
const twitterConfig = require('../config/twitter_config.json');
var twitter = new Twitter(twitterConfig);

exports.searchTwitts = function(req, res) {
    var query = req.params.q;
    console.log(query);

    if(query) twitter.getSearch(
        { q: query, lang: 'fr' }, 
        resp => { // error 
            res.send('error');
        }, 
        resp => { // success
            res.send(JSON.stringify(resp));
        });
};

exports.indexPage = function(req, res) {
    res.sendFile(__basedir + '/client/index.html');
};