const md5 = require('md5');
var mongoose = require('mongoose');
const Twitter = require('twitter-node-client').Twitter;
const twitterConfig = require('../config/twitter_config.json');

var twitter = new Twitter(twitterConfig);
User = mongoose.model('Users');

exports.authUser = function (req, res) {
    User.findOne({user: req.body.user.user, password: md5(req.body.user.password)}, function(err, user) {
        if (err) res.send(err);
        if(user) res.json({ user: user.user, name: user.name, avatar: user.avatar, saved_tweets: user.saved_tweets });
    });
}

exports.createUser = function (req, res) {
    var reqUser = req.body.user;
    var password = md5(reqUser.password)
    var user = new User();

    user.user = reqUser.user;
    user.password = password;
    user.name = reqUser.name;

    if (reqUser.twitter_account) twitter.getUser(
        { screen_name: reqUser.twitter_account },
        resp => { // error 
            res.send('error');
        },
        resp => { // success
            user.twitter_account = reqUser.twitter_account;
            user.avatar = JSON.parse(resp).profile_image_url;

            user.save(function(err, new_user) {
                if(err) res.send(err);
                res.send(new_user.id);
            });

        });
    else {
        user.avatar = '../assets/images/users/default-user-image.png';
        user.twitter_account = '';

        user.save(function(err, new_user) {
            if(err) res.send(err);
            res.send(new_user.id);
        });
    }
}

exports.saveSubject = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            if(user.saved_subjects.findIndex(element => { return element.title == req.body.subject.title }) == -1) {
                user.saved_subjects.push(req.body.subject);
                user.save(function(err, new_user) {
                    if(err) res.send(err);
                    res.send('ok');
                });
            }
            else {
                res.send('Le sujet a déjà été sauvegardé');
            }
        }
    });
}

exports.getSavedSubjects = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            res.send(user.saved_subjects);
        }
    });
}

exports.deleteSavedSubject = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            var index = user.saved_subjects.findIndex(element => { return element.title == req.body.subject });
            if(index != -1)  {
                user.saved_subjects.splice(index, 1);
                user.save(function(err, new_user) {
                    if(err) res.send(err); 
                    res.send('OK')
                });
            }
            else res.send('Sujet non trouvé');
        }
        else res.send('L\'utilisateur spécifié n\'existe pas ('+req.params.user+')');
    });
}

exports.saveTweet = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            if(user.saved_tweets.findIndex(element => { return element.id == req.body.tweet.id }) == -1) {
                user.saved_tweets.push(req.body.tweet);
                user.save(function(err, new_user) {
                    if(err) res.send(err);
                    res.send('ok');
                });
            }
            else {
                res.send('Le tweet a déjà été sauvegardé');
            }
        }
    });
}

exports.getSavedTweets = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            res.send(user.saved_tweets);
        }
    });
}

exports.saveAccount = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            if(user.saved_accounts.findIndex(element => { return element.twitterRef == req.body.account.twitter }) == -1) {
                console.log(req.body.account.name);
                twitter.getUser(
                    { screen_name: req.body.account.twitter },
                    resp => { // error 
                        console.log(resp);
                        res.send('error');
                    },
                    resp => { // success
                        var account = { 
                            name: req.body.account.name,
                            twitterRef: '@'+req.body.account.twitter,
                            image: JSON.parse(resp).profile_image_url,
                            active: false
                        };

                        console.log(account);
            
                        user.saved_accounts.push(account);
                        user.save(function(err, new_user) {
                            if(err) res.send(err);
                            res.send(account);
                        });
            
                    });
            }
            else {
                res.send('Le compte a déjà été sauvegardé');
            }
        }
    });
}

exports.getSavedAccounts = function(req, res) {
    User.findOne({user: req.params.user}, function(err, user) {
        if (err) res.send(err);
        if(user) {
            res.send(user.saved_accounts);
        }
    });
}