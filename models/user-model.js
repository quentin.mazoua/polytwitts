'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({

    user: {
        type: String,
        required: 'Login of the user is required',
        unique: true
    },
    password: {
        type: String,
        required: 'Password of the user is required'
    },
    name: {
        type: String,
    },
    twitter_account: {
        type: String,
    },
    avatar: {
        type: String
    },
    added_date: {
        type: Date,
        default: Date.now
    },
    saved_tweets: {
        type: Array,
        default: []
    },
    saved_subjects: {
        type: Array,
        default: []
    },
    saved_accounts: {
        type: Array,
        default: []
    }
});

module.exports = mongoose.model('Users', UserSchema);