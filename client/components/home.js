export const Home = {
    data: () => ({
        cards: [
          { title: 'Gilets jaunes', src: '../assets/images/gilets_jaunes.jpg', flex: 12 },
          { title: 'Loi anti-casseurs', src: '../assets/images/loi-anti-casseurs.jpg', flex: 6 },
          { title: 'Revenu universel d\'activité', src: '../assets/images/revenu_universel_activite.jpg', flex: 6 },
          { title: 'Grand débat national', src: '../assets/images/grand_debat.jpg', flex: 6 },
          { title: 'Elections européennes', src: '../assets/images/europe.jpg', flex: 6 }
        ],
        savedCards: []
    }),
    methods: {
        clickedSubject(title) { this.$root.$emit('subject-picked', title) },
        addToFavorite(subject) { this.$root.$emit('add-subject-favorite', subject) },
        removeFromFavorite(subject, index) { this.$root.$emit('remove-subject-favorite', { title: subject, index: index } ) }
    },
    created() {
        this.$store.watch(
            (state, getters) => getters.savedSubjects,
            (newValue, oldValue) => {
                this.savedCards = newValue.slice();
                this.savedCards = JSON.parse(JSON.stringify(this.savedCards));
        });

        this.savedCards = this.$store.state.savedSubjects;
    },
    template: `
        <v-layout justify-center>
            <v-flex xs12 sm6>
                <v-card>
                    <h3 style="margin-left: 5px;">Sujets d'actualité</h3>
                    <v-container fluid grid-list-md>
                        <v-layout row wrap>
                            <v-flex v-for="card in cards" :key="card.title">
                                <v-card hover @click="clickedSubject(card.title)">
                                    <v-img :src="card.src" height="200px">
                                        <v-container fill-height fluid pa-2>
                                            <v-layout fill-height>
                                                <v-flex xs12 align-end flexbox>
                                                    <span class="headline white--text" v-text="card.title"></span>
                                                </v-flex>
                                            </v-layout>
                                        </v-container>
                                    </v-img>

                                    <v-card-actions>
                                        <v-spacer></v-spacer>
                                        <v-tooltip bottom>
                                            <template v-slot:activator="{ on }">
                                                <v-btn icon v-on="on" @click.stop="addToFavorite(card)">
                                                    <v-icon>bookmark</v-icon>
                                                </v-btn>
                                            </template>
                                            <span>Ajouter aux favoris</span>
                                        </v-tooltip>
                                    </v-card-actions>
                                </v-card>
                            </v-flex>
                        </v-layout>
                    </v-container>
                    <h3 style="margin-left: 5px;">Sujets sauvegardés</h3>
                    <p v-if="savedCards.length == 0" style="display: block; margin: 0 auto; width: -moz-fit-content; width: fit-content;">Aucun sujet sauvegardé</p>
                    <v-container fluid grid-list-md>
                        <v-layout row wrap>
                            <v-flex v-for="(card, index) in savedCards" :key="card.title">
                                <v-card hover @click="clickedSubject(card.title)">
                                    <v-img :src="card.src" height="200px">
                                        <v-container fill-height fluid pa-2>
                                            <v-layout fill-height>
                                                <v-flex xs12 align-end flexbox>
                                                    <span class="headline white--text" v-text="card.title"></span>
                                                </v-flex>
                                            </v-layout>
                                        </v-container>
                                    </v-img>

                                    <v-card-actions>
                                        <v-spacer></v-spacer>
                                        <v-tooltip bottom>
                                            <template v-slot:activator="{ on }">
                                                <v-btn icon v-on="on" @click.stop="removeFromFavorite(card.title, index)">
                                                    <v-icon>delete</v-icon>
                                                </v-btn>
                                            </template>
                                            <span>Supprimer des favoris</span>
                                        </v-tooltip>
                                    </v-card-actions>
                                </v-card>
                            </v-flex>
                        </v-layout>
                    </v-container>
                </v-card>
            </v-flex>
        </v-layout>
    `    
}