export const TweetsList = { 
    template: `
        <v-container fluid grid-list-md>
            <v-layout row wrap>
                <v-flex v-for="tweet in tweets" :key="tweet.id">
                    <v-card class="mx-auto primary" color="" dark max-width="350">
                        <v-card-title>
                        <v-img style="margin-right: 5px; border-radius: 50%; height: 40px; width: 40px; flex: 0 0 auto !important;" class="elevation-6" v-bind:src="tweet.user.profileImage"></v-img>
                            <v-list-tile-title style="max-width: 150px; text-overflow: ellipsis;">{{tweet.user.name}}</v-list-tile-title>
                            <v-menu offset-y>
                                <template v-slot:activator="{ on }">
                                    <v-btn flat fab color="normal" v-on="on" style="margin-left: auto; font-size: 1.7em;">&#8226;&#8226;&#8226;</v-btn>
                                </template>
                                <v-list>
                                    <v-list-tile @click="saveTweet(tweet)">
                                        <v-list-tile-title><v-icon style="margin-right: 5px;">far fa-save</v-icon>Sauvegarder</v-list-tile-title>
                                    </v-list-tile>
                                    <v-list-tile @click="showMap(tweet)">
                                        <v-list-tile-title><v-icon style="margin-right: 5px;">fas fa-search-location</v-icon>Localisation</v-list-tile-title>
                                    </v-list-tile>
                                </v-list>
                            </v-menu>
                        </v-card-title>
        
                        <v-card-text class="font-weight-bold">
                            {{tweet.text}}
                        </v-card-text>
        
                        <v-card-actions>
                            <v-list-tile class="grow">
                                <v-layout align-center justify-end>
                                    <v-icon class="mr-1">fas fa-heart</v-icon>
                                    <span class="subheading mr-2">{{tweet.favoriteCount}}</span>
                                    <span class="mr-1">·</span>
                                    <v-icon class="mr-1">fas fa-retweet</v-icon>
                                    <span class="subheading">{{tweet.retweetCount}}</span>
                                </v-layout>
                            </v-list-tile>
                        </v-card-actions>
                    </v-card>
                </v-flex>
            </v-layout>
        </v-container>
    `,
    data: () => ({
        tweets: Array
    }),
    methods: {
        saveTweet(tweet) {
            this.$root.$emit('save-tweet', tweet);
        },
        showMap(tweet) {
            this.$root.$emit('show-map', tweet);
        }
    },
    created() {
        this.$store.watch(
            (state, getters) => getters.tweets,
            (newValue, oldValue) => {
                this.tweets = newValue.slice();
                this.tweets = JSON.parse(JSON.stringify(this.tweets));
        });
    },
}