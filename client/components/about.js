export const About = { 
    template: `
        <div>
            <p>Application développée dans le cadre du cours de Programmation Web avancée - IUT de Lannion</p>
            <p>Serveur: NodeJS/Express - MongoDB</p>
            <p>Client: VueJS</p>
            <p>Quentin Mazoua - Mars 2019</p>
        </div>
    `
}