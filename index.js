'use strict';

global.__basedir = __dirname;
const express = require('express');
const routes = require('./config/routes.js');
const app = express();
const logs = require('./utils/logs.js');
const expressip = require('express-ip');
const Users = require('./models/user-model.js');
var mongoose = require('mongoose');
const dbConfig = require('./config/db.json');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

global.dbConfig = dbConfig;

app.use(expressip().getIpInfoMiddleware);

app.use('/*', function (req, res, next) {
    logs.logRequest(req);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    
    next();
});

app.use(express.static('client'));

routes(app);

mongoose.Promise = global.Promise;
mongoose.connect(global.dbConfig.mongoURL, { useNewUrlParser: true });

app.listen(3000, () => {
    console.log('Server started on port 3000');
});