'use strict';

exports.logRequest = function(req) {
    const fs = require('fs');
    const ipInfo = req.ipInfo;
    const filePath = __basedir + '/logs/requests.log';
    var log = new Date().toLocaleString() + (!ipInfo.error ? (' | ' + ipInfo.country + ipInfo.city) : '') + ' => ' + req._parsedUrl.path + '\r\n';

    try {
        if (fs.existsSync(filePath)) {
            fs.appendFile(filePath, log, (err) => {  
                if (err) throw err;
            });
        }
        else {
            fs.writeFile(filePath, log, (err) => {  
                if (err) throw err;
            });
        }
    } catch(err) {
        console.error(err);
    }
};